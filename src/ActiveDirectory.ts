import type { SearchOptions } from 'ldapjs';
import { getAllGroups } from './actions/getAllGroups';
import { getAllUsers } from './actions/getAllUsers';
import { loginUser } from './actions/loginUser';
import { Config } from './Config';

/** This class will authenticate a user to AD and return basic user information */
export class ActiveDirectory {
  readonly config: Config

  /**
   * Create a new Active Directory object for manging AD connections
   * @param config Configuration options
   */
  constructor(config: { ldapjs: Pick<Config['ldapjs'], 'url'> & Partial<Config['ldapjs']> } & Partial<Omit<Config, 'ldapjs'>>) {
    this.config = { base: '', searchOptions: {}, ...config, ldapjs: { idleTimeout: 30000, tlsOptions: '', ...config['ldapjs'] } }
  }
  /**
   * Attempts to authenticate 1 user to AD using their UPN.
   * If the ldap client has an error a user friendly message is in message and the full error is in error.
   * @param username This must be the UPN
   * @param password The users password
   * @param customBase Override the default class base, if not passed the class base is used.
   * @param customSearch A custom search string, e.g. (userPrincipalName=test@domain.local)
   * @returns Promise resolves as an obj { success: true, entry: {} || undefined } || { success: false, message: 'error', error: 'ldapjs error' }
   */
  public async loginUser(username: string, password: string, customBase: string = this.config.base, customSearch: SearchOptions = {}) {
    return await loginUser(this.config, username, password, customBase, customSearch)
  }

  /**
   * Attempts to get all users from AD that the user has permissions to read and match filter.
   * @param config The config option
   * @param username This must be the UPN
   * @param password The users password
   * @param customBase Override the default class base, if not passed the class base is used.
   * @param formatted Indicates if you;d like your response formatted as user objects
   * @returns Promise resolves as an obj { success: true, users: [object] } || { success: false, message: 'error', error: 'ldapjs error' }
   */
  public async getAllUsers(username: string, password: string, customBase: string = this.config.base, formatted: boolean) {
    return await getAllUsers(this.config,username,password,customBase,formatted)
  }
  /**
   * Attempts to get all groups from AD that the user has permissions to read and match filter.
   * @param username This must be the UPN
   * @param password The users password
   * @param customBase Override the default class base, if not passed the class base is used.
   * @param detailed Indicates if you want the detailed groups objects with name, dn, guid, description, created, and changed values
   * @returns Promise resolves as an obj { success: true, groups: [string] } || { success: false, message: 'error', error: 'ldapjs error' }
   */
  public async getAllGroups(username: string, password: string, customBase: string = this.config.base, detailed: boolean = true) {
    return await getAllGroups(this.config, username, password, customBase, detailed)
  }
}

