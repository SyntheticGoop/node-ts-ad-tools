/**
 * Detects what type of account name this is or defaults to userLogonName
 * @param username The user name being used to bind
 * @returns Returns userPrincipalName || distinguishedName || sAMAccountName
 */
export function detectLogonType(username: string) {
  if (username.indexOf('@') !== -1) {
    return 'userPrincipalName';
  } else if (username.toUpperCase().indexOf('DC=') !== -1) {
    return 'distinguishedName';
  } else {
    return 'sAMAccountName';
  }
}
