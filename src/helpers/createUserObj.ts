import type { SearchEntry } from 'ldapjs';
import { resolveGroups } from './resolveGroups';
import { resolveGUID } from './resolveGUID';

/**
 * Creates a standard user object from ldapjs entry response
 * @param entry This is an entry returned from loginAdUser
 * @returns User object { groups: Array, phone: string, name: string, mail: string, guid: string }
 */
export function createUserObj(entry: Pick<SearchEntry, 'object' | 'attributes' | 'objectName'>) {
  if (typeof entry !== 'object')
    throw new Error('Entry must be an object');

  return {
    groups: resolveGroups(entry),
    phone: entry.object.telephoneNumber || '',
    name: entry.object.name || '',
    mail: entry.object.mail || '',
    guid: resolveGUID(entry),
    dn: entry.objectName
  };
}
