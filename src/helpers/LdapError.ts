export class LdapError extends Error {
  public lde_message?: string;

  constructor(error: string | { name: string, lde_message: string }) {
    super(typeof error === 'string' ? error : error.name)
    if (typeof error !== 'string') {
      this.name =error.name
      this.lde_message = error.lde_message
    } else {
      this.name = error
    }
  }

  /**
   * Turns AD bind errors into friendlier error messages
   * @returns Error explanation string
   */
  public get friendly(): string {

    if (this.name !== 'InvalidCredentialsError' || !this.lde_message)
      return 'Unknown Auth Error'

    if (this.lde_message.indexOf('775') !== -1)
      return 'Account is locked out';

    return 'Invalid username or password';
  }
}
