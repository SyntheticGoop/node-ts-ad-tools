import type { Client } from 'ldapjs';
import { LdapError } from "./LdapError";

/**
 * Performs a bind on the client passed in
 * @param client LDAPjs client obj
 * @param username Username to bind with
 * @param password Password to bind with
 * @returns Resolvs with LDAPjs response
 * @throws If username or password are not a string
 */
export function bind(client: Client, username: string, password: string) {
  if (typeof username !== 'string' || typeof password !== 'string') {
    const err = new LdapError('Usrname and password must be a string');
    err.name = 'InvalidCredentialsError';
    err.lde_message = 'Usrname and password must be a string';
    throw err;
  }

  return new Promise((resolve, reject) => {
    client.bind(username, password, (err, res) => {
      if (err) {
        reject(err);
        return;
      }

      resolve(res);
    });
  });
}
