/**
 * Cleans sAMAccountName
 * @param sAMA
 * @returns sAMAccountName
 */
export function cleanSama(sAMA: string) {
  const parts = sAMA.split('\\'); // Extracts any appended domain
  return parts[parts.length - 1]; // This returns 0 if there was no domain provided or returns 1 if domain was provided
}
