/**
 * Converts the ActiveDirectory / LDAP fields whenCreated & whenChanged to JS dates
 * @param date Date
 * @returns ISO formatted date
 */
export function convertToDate(date: string | string[]) {
  const year = date.slice(0, 4);
  const month = date.slice(4, 6);
  const day = date.slice(6, 8);
  const hour = date.slice(8, 10);
  const min = date.slice(10, 12);
  const sec = date.slice(12, 14);

  return new Date(`${year}-${month}-${day}T${hour}:${min}:${sec}.000Z`);
}
