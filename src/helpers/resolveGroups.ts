import type { SearchEntry } from 'ldapjs';

/**
 * Resolves AD group membership
 * @param entry This is an entry returned from loginAdUser
 * @returns An array of string group names
 */
export function resolveGroups(entry: Pick<SearchEntry, 'object'>) {
  if (typeof entry.object !== 'object')
    throw new Error('Invalid entry, entry.object must be an object');

  const memberOf = entry.object.memberOf;
  if (memberOf === undefined) {
    return [];
  } else if (typeof memberOf === 'string') {
    // If only 1 OU ldapjs returns it as a string
    return memberOf.split(',')
      .filter(item => item.indexOf('CN=') !== -1)
      .map(item => item.split('CN=')[1]);

  } else if (Array.isArray(memberOf)) {
    return memberOf.map(group => group.split(',')[0].split('CN=')[1]);
  }

  return [];
}

