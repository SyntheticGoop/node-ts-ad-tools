import { LdapError } from './LdapError';

export function formatError(err: unknown) {
  return { message: err instanceof LdapError ? err.friendly : err instanceof Error ? err.message : typeof err === 'string' ? err : '', error: err };
}
