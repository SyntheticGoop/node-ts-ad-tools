import type { Client, SearchEntry, SearchOptions } from 'ldapjs';

/**
 * Performs a search on a client
 * @param client LDAPjs client obj
 * @param base The base to perform the search on
 * @param search The search options to use
 */
export function search(client: Client, base: string, search: SearchOptions) {
  return new Promise<SearchEntry[]>((resolve, reject) => {
    let accumulator: SearchEntry[] = [];
    //TODO Add check if client is bind
    client.search(base, search, (error, res) => {
      if (error) {
        reject(error);
        return;
      }

      res.on('searchEntry', entry => {
        accumulator.push(entry);
      });

      res.on('end', res => {
        resolve(accumulator);
        return;
      });

      res.on('error', error => {
        reject(error);
        return;
      });
    });
  });
}
