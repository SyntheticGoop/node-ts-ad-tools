import type { SearchOptions } from 'ldapjs';

export type Config = {
  /** ldapjs settings */
  ldapjs: {
    /** URL / IP of the Active Directory server */
    url: string;
    /** The NodeJS TLS options object, see https://nodejs.org/api/tls.html#tls_tls_connect_options_callback for details. */
    tlsOptions: {};
    /** How long to wait idle before timing out*/
    idleTimeout: number;
  }
  /** The default base to use when one is not provided on a method */
  base: string;
  /** 
   * ldapjs SearchOptions
   *   - `scope` What scope should the Active Directory be searched in
   *   - `filter` A string version of an LDAP filter (eg. 'objectclass=*')
   *   - `sizeLimit` Maximum number of entries to return, 0 = unlimited
   */
  searchOptions: SearchOptions;
};
