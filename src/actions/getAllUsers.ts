import ldap from 'ldapjs';
import type { Config } from '../Config';
import { bind } from '../helpers/bind';
import { createUserObj } from '../helpers/createUserObj';
import { formatError } from '../helpers/formatError';
import { search } from '../helpers/search';

/**
 * Attempts to get all users from AD that the user has permissions to read and match filter.
 * @param config The config option
 * @param username This must be the UPN
 * @param password The users password
 * @param customBase Override the default class base, if not passed the class base is used.
 * @param formatted Indicates if you;d like your response formatted as user objects
 * @returns Promise resolves as an obj { success: true, users: [object] } || { success: false, message: 'error', error: 'ldapjs error' }
 */

export async function getAllUsers(config: Config, username: string, password: string, customBase: string, formatted: boolean) {
  const client = ldap.createClient(config.ldapjs);

  // Return errors
  client.on('error', error => {
    client.unbind();
    return { success: false, message: 'Error resolving users', error };
  });

  try {
    const searchOptions = {
      ...config.searchOptions,
      filter: `(&(objectClass=user)(objectCategory=person))`
    };

    // Bind to AD - error thrown if invalid login
    await bind(client, username, password);

    // Search AD for groups
    let records = await search(client, customBase.trim().length > 0 ? customBase : config.base, searchOptions);

    return {
      success: true,
      // Create formatted AD users if user requested them
      users: formatted ? records.map(entry => createUserObj(entry)) : records
    };
  } catch (err) {
    return { success: false, ...formatError(err) };
  } finally {

    client.destroy()

  }
}
