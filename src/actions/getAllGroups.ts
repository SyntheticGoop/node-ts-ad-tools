import ldap from 'ldapjs';
import type { Config } from '../Config';
import { bind } from '../helpers/bind';
import { convertToDate } from '../helpers/convertToDate';
import { formatError } from '../helpers/formatError';
import { resolveGUID } from '../helpers/resolveGUID';
import { search } from '../helpers/search';

/**
 * Attempts to get all groups from AD that the user has permissions to read and match filter.
 * @param config The config option
 * @param username This must be the UPN
 * @param password The users password
 * @param customBase Override the default class base, if not passed the class base is used.
 * @param detailed Indicates if you want the detailed groups objects with name, dn, guid, description, created, and changed values
 * @returns Promise resolves as an obj { success: true, groups: [string] } || { success: false, message: 'error', error: 'ldapjs error' }
 */

export async function getAllGroups(config: Config, username: string, password: string, customBase: string, detailed: boolean) {
  const client = ldap.createClient(config.ldapjs);

  // Return errors
  client.on('error', error => {
    client.unbind();
    return { success: false, message: 'Error resolving groups', error };
  });

  try {
    const attributes = detailed ? ['name', 'dn', 'objectGUID', 'description', 'whenCreated', 'whenChanged'] : 'name';
    const searchOptions = {
      ...config.searchOptions,
      filter: `(objectCategory=group)`,
      attributes
    };

    // Bind to AD - error thrown if invalid login
    await bind(client, username, password);

    // Search AD for groups
    const records = await search(client, customBase.trim().length > 0 ? customBase : config.base, searchOptions);

    function getDetails(records: ReturnType<typeof search> extends Promise<infer T> ? T : never) {
      return records.map(entry => {
        return {
          name: entry.object.name,
          dn: entry.object.dn,
          guid: resolveGUID(entry),
          description: entry.object.description,
          created: convertToDate(entry.object.whenCreated),
          changed: convertToDate(entry.object.whenChanged)
        };
      });
    }

    return {
      success: true,
      groups: detailed ? getDetails(records) : records.map(entry => entry.object.name)
    };

  } catch (err: unknown) {
    return { success: false, ...formatError(err) };
  } finally {

    client.destroy()

  }
}
