import type { SearchOptions } from 'ldapjs';
import ldap from 'ldapjs';
import type { Config } from '../Config';
import { bind } from '../helpers/bind';
import { cleanSama } from '../helpers/cleanSama';
import { detectLogonType } from '../helpers/detectLogonType';
import { formatError } from '../helpers/formatError';
import { search } from '../helpers/search';

/**
 * Attempts to authenticate 1 user to AD using their UPN.
 * If the ldap client has an error a user friendly message is in message and the full error is in error.
 * @param config The config option
 * @param username This must be the UPN
 * @param password The users password
 * @param customBase Override the default class base, if not passed the class base is used.
 * @param customSearch A custom search string, e.g. (userPrincipalName=test@domain.local)
 * @returns Promise resolves as an obj { success: true, entry: {} || undefined } || { success: false, message: 'error', error: 'ldapjs error' }
 */

export async function loginUser(config: Config, username: string, password: string, customBase: string, customSearch: SearchOptions) {
  const client = ldap.createClient(config.ldapjs);

  // Return errors
  client.on('error', error => {
    client.unbind();
    return { success: false, message: 'Error resolving account', error };
  });

  try {
    const usernameType = detectLogonType(username);
    const searchUser = usernameType === 'sAMAccountName' ? cleanSama(username) : username;
    const searchOptions = {
      ...config.searchOptions,
      filter: `(${usernameType}=${searchUser})`,
      ...customSearch // Overrides any other search options
    };


    // Bind to AD - error thrown if invalid login
    await bind(client, username, password);

    // Search AD for user
    const records = await search(client, customBase.trim().length > 0 ? customBase : config.base, searchOptions);

    // We only expect 1 user acct
    return { success: true, entry: records[0] };
  } catch (err: unknown) {
    return { success: false, ...formatError(err) };
  } finally {

    client.destroy()

  }
}
