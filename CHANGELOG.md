# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.3.4](https://github.com/tastypackets/node-ad-tools/compare/v1.3.3...v1.3.4) (2021-02-05)


### Bug Fixes

* Client should be cleaned up on both success and failure ([d445d53](https://github.com/tastypackets/node-ad-tools/commit/d445d53ec6810fb99b1b4e0caa185fdc2aefb976))

### [1.3.3](https://github.com/tastypackets/node-ad-tools/compare/v1.3.2...v1.3.3) (2021-02-05)


### Bug Fixes

* Client should be cleaned up and destroyed after use ([2e62d71](https://github.com/tastypackets/node-ad-tools/commit/2e62d71aa61492d05d0626746dc4830599a10cdf))

### [1.3.2](https://github.com/tastypackets/node-ad-tools/compare/v1.3.1...v1.3.2) (2021-02-05)


### Bug Fixes

* **loginUser:** Return type should not be generic ([0367fda](https://github.com/tastypackets/node-ad-tools/commit/0367fdab47edf1584248cbe59d33722e1593cf91))

### 1.3.1 (2021-02-05)


### Features

* Migrate to typescript ([60e2b92](https://github.com/tastypackets/node-ad-tools/commit/60e2b92ad1210679b60a4e80cc8a264247361643))


### Bug Fixes

* base should default to config ([fe28e93](https://github.com/tastypackets/node-ad-tools/commit/fe28e93145bfd5c1f4d1efaf96f9eb9ab5169815))
